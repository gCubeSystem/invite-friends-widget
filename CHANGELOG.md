
# Changelog for workspace

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.6.2] - 2022-07-12

 - Ported to git and GWT 2.8.2

## [v1.5.0] - 2017-11-29"

Ported to GWT 2.8.11

Removed ASL session

fix for Bug #518 Wrong port in redirect email links (9090)

Feature #520 Invite Friends widget to make user entered email
			address lowercase


## [v1.0.0] - 2015-07-02

First release 
